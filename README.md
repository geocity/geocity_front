# Geocity Front

## Generic tools

In order to build the project, you'll need *Yarn* and *NodeJS* installed.

### Yarn installation

Please refer to [Yarn Documentation](https://classic.yarnpkg.com/en/docs/install).
On Windows, install *Chocolatey* when prompted.

### Node version

If you have `nvm` installed on your system, you can simply run `nvm use` in the root directory to pick up the correct version specified in the `.nvmrc` file.

## Build Setup & Development

```bash
# Initialize submodules dependencies
git submodule update --init --recursive
```

```bash
# Install dependencies
yarn install

# Serve with hot reload at localhost:3000
yarn dev

# Build the static project
yarn build
```

For detailed explanations on how things work, check out the [Nuxt.js docs](https://nuxtjs.org).

## Mock Service Worker

To enable MSW, change the value of your `MSW` environment variable to `enable`.

## Configuration file

Additional configuration examples for deployment are available under `/deploy_configurations`.

```bash
# Create your env. file
cp .env.example .env
```

Also make sure to create the necessary locale files.

```bash
# Create your locale JSON files
for f in locales/*.json.example; do cp "$f" "${f%.example}"; done
```

The `.env` file must contain:

### 🚨 Mandatory API URLs

The main GeoCity API:

```
GEOCITY_API=https://form-preprod.mapnv.ch/rest
```

The Location API:

```
LOCATION_API=https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=yverdon-les-bains&type=locations
```

### 🚀 For production

Needed if the application is not deployed on the root:

```
PRODUCTION_PATH=/your-custom-path/
```

### 🔍 Filtering events from GeoCity API

**📅 First event date:**

```
GEOCITY_API_EVENTS_START=2020-01-01
```

**📅 Last event date:**

```
GEOCITY_API_EVENTS_END=2050-12-01
```

**📅 Only display future events:**

```
GEOCITY_API_SHOW_ONLY_FUTURE=true
```

**🏢 Community/City/Office for which events are displayed:**

```
GEOCITY_API_ADMINISTRATIVE_ENTITES=3
```

### 🔗 Link to the Django app login page

```
CTA_LINK=
```

### 👀 Show or hide the header and footer

```
DISPLAY_FOOTER_AND_HEADER=true
```

### 🌍 / 📅 Choose the default view (`calendar` or `map`)

```
DEFAULT_VIEW='calendar'
```

### 🌍 Set the default map location

```
DEFAULT_MAP_X=2537000
DEFAULT_MAP_Y=1162000
DEFAULT_MAP_ZOOM=4
```

### 📅 Choose the default calendar visualization mode

Possible values are: `default`, `timeGridDay`, `timeGridWeek`, `dayGridMonth`, `listMonth`.

```
DEFAULT_CALENDAR_MODE='default'
```

## Release

The `CHANGELOG.md` file is automatically updated at the root of the project based on commit messages, as long as they follow the [Angular commit guidelines](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines). It will also bump the version number in the `package.json`, commit, tag, and push the changes. This process is interactive, and you'll be able to skip steps manually if needed.

To release a new version, run:

```bash
yarn release [patch|minor|major|version_number]
```

By default, the version bump is automatically determined based on commit messages.

For more information, check out the [release-it documentation](https://github.com/webpro/release-it).

## Deploy

To deploy a build of the project, simply replace the blank command in the `package.json`, under `scripts -> deploy`. This will be automatically invoked at the end of the release process described above.

Example of a simple deploy command using `rsync`:

```json
"deploy": "rsync -avz --delete --exclude='.*' dist/ user@server.com:/var/www/html/my-project"
```

You can also deploy manually at any time by running:

```bash
yarn deploy
```

🚨 **Notice:** The `--delete` flag means all files not present locally will be deleted on the remote server. Be careful, this can lead to data loss!

🚨 Make sure the static server file is listed in the `ALLOWED_CORS` environment variable in the backend config:
[Example backend configuration](https://gitlab.com/geocity/geocity/-/blob/main/.env.example)
